// HotPlate.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <fstream>


using namespace std;

// Set how big the plate is going to be
const int rows = 20;
const int columns = 20;

// Set the beginning conditions for the hot plate
void initializeHotPlate(double hotPlate[][columns])
{
	for (int currentRow = 0; currentRow < rows; currentRow++)
	{
		for (int currentColumn = 0; currentColumn < columns; currentColumn++)
		{
			// Check if we are at the top or bottom rows
			if (currentRow == 0 || currentRow == rows - 1)
			{
				// Then make sure we aren't at the beginning or end columns
				if (currentColumn == 0 || currentColumn == columns - 1)
				{
					// The elements on the corner
					hotPlate[currentRow][currentColumn] = 0;
				}
				else
				{
					// The outside elements not on the corner
					hotPlate[currentRow][currentColumn] = 100.0;
				}

			}
			else
			{
				// Everything else
				hotPlate[currentRow][currentColumn] = 0;
			}
		}
	}
}

void printHotPlate(double hotPlate[][columns])
{
	// Print out the array
	for (int currentRow = 0; currentRow < rows; currentRow++)
	{
		for (int currentColumn = 0; currentColumn < columns; currentColumn++)
		{
			if (currentColumn == columns - 1)
			{
				cout << fixed << setprecision(1) << hotPlate[currentRow][currentColumn] << "\n";
			}
			else
			{
				cout << fixed << setprecision(1) << hotPlate[currentRow][currentColumn] << ",";
			}
		}
	}
}

// Normalize the hot plate
double averageHotPlate(double hotPlate[][columns])
{
	const int numbOfElementsToAverage = 4;

		double largestChange = 0;
		// Do an averaging of for all elements in hotplate (excluding edges)
		for (int currentRow = 1; currentRow < rows - 1; currentRow++)
		{
			for (int currentColumn = 1; currentColumn < columns - 1; currentColumn++)
			{
				// Add up the values of the elements above, below, to the left, and to the right of the current element
				double sum = hotPlate[currentRow - 1][currentColumn] + hotPlate[currentRow + 1][currentColumn] + hotPlate[currentRow][currentColumn + 1] + hotPlate[currentRow][currentColumn - 1];

				// Then divide by the number of elements we added together
				double average = sum / numbOfElementsToAverage;

				// See by how many degrees the hot plate changed
				double currentChange = fabs(average - hotPlate[currentRow][currentColumn]);

				// Check to see if the previous largest change is smaller than the change in the current element
				if (currentChange > largestChange)
				{
					// Make the largest change equal to the change in the current element
					largestChange = currentChange;
				}
				// This sets the hotplate's new value
				hotPlate[currentRow][currentColumn] = average;
			}
		}
		return largestChange;
}



void exportHotPlate(double hotPlate[][columns])
{
	// Export array to excel
	ofstream myfile("hotplateprint.csv");
	if (myfile.is_open())
	{
		for (int currentRow = 0; currentRow < rows; currentRow++)
		{
			for (int currentColumn = 0; currentColumn < columns; currentColumn++)
			{
				if (currentColumn == columns - 1)
				{
					myfile << fixed << setprecision(4) << hotPlate[currentRow][currentColumn] << "\n";
				}
				else
				{
					myfile << fixed << setprecision(4) << hotPlate[currentRow][currentColumn] << ",";
				}

			}
		}
		myfile.close();
	}
	else
	{
		cout << "Unable to open file";
	}
}

int main()
{
	// Create the plate
	double hotPlate[rows][columns];

	initializeHotPlate(hotPlate);
	printHotPlate(hotPlate);
	cout << endl << endl;
	const double maxChangeValueToStop = .1;
	bool finishedAveraging = false;
	int counter = 0;
	while (!finishedAveraging)
	{
		double largestChange = averageHotPlate(hotPlate);
		// See if the steady state has been reached
		if (largestChange <= maxChangeValueToStop)
		{
			// Quit if it has
			finishedAveraging = true;
		}
		if (counter == 0)
		{
			printHotPlate(hotPlate);
			cout << endl << endl;
		}
		counter++;
	}
	printHotPlate(hotPlate);
	exportHotPlate(hotPlate);


	system("pause");
	return 0;
}